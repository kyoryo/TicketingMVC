﻿using Domain.Infrastructure;
using Scaffold;
using Scaffold.Models.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    [TrackChanges()]
    public class Ticket : IModel<int>, ISoftDeletable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [DisplayName("Subject")]
        public string Title { get; set; }

        [UIHint("RichTextBox")]
        public string Description { get; set; }

        #region category
        [DisplayName("Category")]
        public int? TicketCategoryId { get; set; }
        [DisplayName("Application")]
        public int? TicketApplicationId { get; set; }
        [DisplayName("Status")]
        public int? TicketStatusId { get; set; }
        [DisplayName("Priority")]
        public int? TicketPriorityId { get; set; }
        #endregion

        [DisplayName("Due Date")]
        public DateTime? DueDate { get; set; }

        public string CreatorId { get; set; }

        public int? SolutionId { get; set; }

        [ForeignKey("CreatorId")]
        public virtual ApplicationUser Creator { get; set; }

        [ForeignKey("SolutionId")]
        public virtual Solution Solution { get; set; }

        public virtual IList<TicketComment> Comments { get; set; }

        /// <summary>
        /// ticket enum replacer: 1 ticket have 1 category
        /// </summary>
        #region ticket category
        //[ToTS(TSFlag.Ignore)]
        [ForeignKey("TicketCategoryId")]
        public virtual TicketCategory TicketCategory { get; set; }
        //[ToTS(TSFlag.Ignore)]
        [ForeignKey("TicketStatusId")]
        public virtual TicketStatus TicketStatus { get; set; }
        //[ToTS(TSFlag.Ignore)]
        [ForeignKey("TicketApplicationId")]
        public virtual TicketApplication TicketApplication { get; set; }
        //[ToTS(TSFlag.Ignore)]
        [ForeignKey("TicketPriorityId")]
        public virtual TicketPriority TicketPriority { get; set; }
        #endregion

        public DateTime CreatedAt { get; set; }

        public DateTime? LastUpdateTime { get; set; }

        public bool IsDeleted { get; set; }

        public Ticket()
        {
            this.CreatedAt = DateTime.UtcNow;
        }
    }

}
