﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.Interface
{
    public interface ITicketComment
    {
        IQueryable<TicketComment> TiketComment { get; }
        void SaveTicketComment(IList<TicketComment> lstTicketComment);
        IList<Models.TicketComment> GetTicketComment();
    }
    
}
