﻿using Domain.Infrastructure;
using Scaffold.Models.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class TicketComment : IModel<Guid>, ISoftDeletable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public int TicketId { get; set; }

        [Required]
        public string Content { get; set; }

        public string CommenterId { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? LastUpdateTime { get; set; }

        public bool IsDeleted { get; set; }

        [ForeignKey("TicketId")]
        public virtual Ticket Ticket { get; set; }

        [ForeignKey("CommenterId")]
        public virtual ApplicationUser Commenter { get; set; }
    }

}
