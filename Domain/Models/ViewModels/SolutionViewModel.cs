﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;
using Domain.Infrastructure;

namespace Domain.Models.ViewModels
{
    public class SolutionViewModel : IMapFrom<Solution>, IHaveCustomMappings
    {
        public ApplicationUser Creator { get; set; }
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        public string CreatorId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        public bool IsPublished { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Solution, SolutionViewModel>();
        }

        public virtual Solution ToEntity()
        {
            return new Solution()
            {
                Id = Id,
                Title = Title,
                Content = Content,
                CreatorId = CreatorId,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
                IsPublished = IsPublished,
            };
        }
    }
}

