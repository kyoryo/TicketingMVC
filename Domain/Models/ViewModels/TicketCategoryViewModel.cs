﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class TicketCategoryViewModel : IMapFrom<TicketCategory>, IHaveCustomMappings
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        [Required]
        public string Name { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<TicketCategory, TicketCategoryViewModel>();
        }

        public virtual TicketCategory ToEntity()
        {
            return new TicketCategory()
            {
                Id = Id,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
                Name = Name,
            };
        }
    }
}

