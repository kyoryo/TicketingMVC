﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class ProductViewModel : IMapFrom<Product>, IHaveCustomMappings
    {
        public Category Category { get; set; }
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Picture { get; set; }
        public string Description { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Product, ProductViewModel>();
        }

        public virtual Product ToEntity()
        {
            return new Product()
            {
                Id = Id,
                Name = Name,
                Picture = Picture,
                Description = Description,
                Price = Price,
                Quantity = Quantity,
                CategoryId = CategoryId,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
            };
        }
    }
}

