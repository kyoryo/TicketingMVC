﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;
using Domain.Infrastructure;

namespace Domain.Models.ViewModels
{
    public class TicketViewModel : IMapFrom<Ticket>, IHaveCustomMappings
    {
        public ApplicationUser Creator { get; set; }
        public Solution Solution { get; set; }
        public TicketApplication TicketApplication { get; set; }
        public TicketCategory TicketCategory { get; set; }
        public TicketPriority TicketPriority { get; set; }
        public TicketStatus TicketStatus { get; set; }
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public int? TicketCategoryId { get; set; }
        public int? TicketApplicationId { get; set; }
        public int? TicketStatusId { get; set; }
        public int? TicketPriorityId { get; set; }
        public DateTime? DueDate { get; set; }
        public string CreatorId { get; set; }
        public int? SolutionId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Ticket, TicketViewModel>();
        }

        public virtual Ticket ToEntity()
        {
            return new Ticket()
            {
                Id = Id,
                Title = Title,
                Description = Description,
                TicketCategoryId = TicketCategoryId,
                TicketApplicationId = TicketApplicationId,
                TicketStatusId = TicketStatusId,
                TicketPriorityId = TicketPriorityId,
                DueDate = DueDate,
                CreatorId = CreatorId,
                SolutionId = SolutionId,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
            };
        }
    }
}

