﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class TicketApplicationViewModel : IMapFrom<TicketApplication>, IHaveCustomMappings
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        [Required]
        public string Name { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<TicketApplication, TicketApplicationViewModel>();
        }

        public virtual TicketApplication ToEntity()
        {
            return new TicketApplication()
            {
                Id = Id,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
                Name = Name,
            };
        }
    }
}

