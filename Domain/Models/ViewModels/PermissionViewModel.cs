﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Domain.Infrastructure;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class PermissionViewModel : IMapFrom<Permission>, IHaveCustomMappings
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Permission, PermissionViewModel>();
        }

        public virtual Permission ToEntity()
        {
            return new Permission()
            {
                Id = Id,
                Name = Name,
                Description = Description,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
            };
        }
    }
}

