﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using Newtonsoft.Json;
using Scaffold;
using Scaffold.Models.Interface;

namespace Domain.Infrastructure
{
    [TrackChanges()]
    public class Permission : IModel<Guid>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [Index("IX_Name_Unique", IsUnique = true)]
        public string Name { get; set; }

        [UIHint("RichTextBox")]
        public string Description { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [Required]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime CreatedAt { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [HiddenInput]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime? LastUpdateTime { get; set; }

        #region Relationship

        [JsonIgnore]
        [ToTS(TSFlag.Ignore)]
        public virtual IList<RolePermission> RolePermissions { get; set; }

        #endregion
    }
}
