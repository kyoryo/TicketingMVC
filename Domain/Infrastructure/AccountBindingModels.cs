﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Helpers;

namespace Domain.Infrastructure
{
    // Models used as parameters to AccountController actions.
    public class UserLoginModel
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }

    public class ResetModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ResetPasswordModel
    {
        [Required]
        public string Id { get; set; }
        
        [Required]
        public string Token { get; set; }
        
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class CreateUserBindingModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Nama")]
        public string Name { get; set; }

        [Display(Name = "Telepon")]
        public string Telephone { get; set; }

        [Display(Name = "Alamat")]
        public string Address { get; set; }

        [Display(Name = "Role Name")]
        public string RoleName { get; set; }

        public string Photo { get; set; }
    }

    public class EditUserAccountBindingModel
    {
        public string Id { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }
    }

    public class EditUserProfileBindingModel
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        [DisplayName("Nama")]
        [MaxLength(200)]
        [Required]
        public string Name { get; set; }

        [DisplayName("Tempat Lahir")]
        [Required]
        public string Birthplace { get; set; }

        [DisplayName("Tanggal Lahir")]
        [Required]
        public DateTime? Birthdate { get; set; }

        [DisplayName("Alamat")]
        [Required]
        public string Address { get; set; }

        [DisplayName("Telepon")]
        [Required]
        public string Telephone { get; set; }

        public string Photo { get; set; }
    }

    public class ChangePasswordBindingModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}