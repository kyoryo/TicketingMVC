﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;
using App.Infrastructure;
using System.Data.OleDb;
using System.Xml;
using System.Configuration;
using System.Data.SqlClient;

namespace App.Controllers
{
    public class TicketEventArgs : EventArgs
    {
        public readonly Ticket After;
        public readonly CrudEventEnumStatus Status;

        public TicketEventArgs(Ticket after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class TicketController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<TicketEventArgs> AfterCall;

        protected virtual void OnCrudOperation(TicketEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }
        //[PermissionAuthorization("read_master_data")]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexUser()
        {
            return View();
        }
        #region testIMPORT
        /// <summary>
        /// actionresult for import - using SQL syntax
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            DataSet ds = new DataSet();
            if (Request.Files["file"].ContentLength > 0)
            {
                string FileExtension = System.IO.Path.GetExtension(Request.Files["file"].FileName);
                if (FileExtension == ".xls" || FileExtension == ".xlsx")
                {
                    string fileLocation = Server.MapPath("~/Content/") + Request.Files["file"].FileName;
                    //if there is a same file name in file location, delete it
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files["file"].SaveAs(fileLocation);

                    //init connection string
                    string excelConnectionString = string.Empty;
                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";

                    //connection string for xls format
                    if (FileExtension == ".xls")
                    {
                        excelConnectionString = "Provider= Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=yes;IMEX=2\"";
                    }
                    //connection string for xlsx format
                    if (FileExtension == ".xlsx")
                    {
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }


                    //create connection to workbook and OLEDB namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelsheets = new String[dt.Rows.Count];
                    int t = 0;

                    //excel data saved in temp files
                    foreach (DataRow row in dt.Rows)
                    {
                        excelsheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);

                    string query = string.Format("Select * from [{0}]", excelsheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                }
                if (FileExtension.ToString().ToLower().Equals(".xml"))
                {
                    string fileLocation = Server.MapPath("~/Content/") + Request.Files["FileUpload"].FileName;
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files["FileUpload"].SaveAs(fileLocation);
                    XmlTextReader xmlreader = new XmlTextReader(fileLocation);

                    ds.ReadXml(xmlreader);
                    xmlreader.Close();
                }
                //conection using SQL
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var _currCreatorId = User.Identity.GetUserId().ToString();
                    var _date = DateTime.UtcNow.ToString();
                    var _solutionId = string.Empty;
                    _solutionId = ds.Tables[0].Rows[i][9].ToString();
                    if (string.IsNullOrWhiteSpace(_solutionId)) { _solutionId = null; }
                    var _isDeleted = string.Empty;
                    _isDeleted = ds.Tables[0].Rows[i][7].ToString();
                    if (string.IsNullOrWhiteSpace(_isDeleted)) { _isDeleted = false.ToString(); }
                    string conn = ConfigurationManager.ConnectionStrings["ScaffoldTR"].ConnectionString;
                    SqlConnection con = new SqlConnection(conn);
                    string query = "SET IDENTITY_INSERT Ticket ON " +
                        "BEGIN TRY INSERT into Ticket(Id, Title, Description, TicketCategoryId, TicketApplicationId, TicketStatusId, TicketPriorityId, DueDate, CreatorId, SolutionId, CreatedAt, LastUpdateTime, IsDeleted) Values('" +
                        ds.Tables[0].Rows[i][0].ToString() + "','" +
                        ds.Tables[0].Rows[i][1].ToString() + "','" +
                        ds.Tables[0].Rows[i][2].ToString() + "','" +
                        ds.Tables[0].Rows[i][3].ToString() + "','" +
                        ds.Tables[0].Rows[i][4].ToString() + "','" +
                        ds.Tables[0].Rows[i][5].ToString() + "','" +
                        ds.Tables[0].Rows[i][6].ToString() + "','" +
                        ds.Tables[0].Rows[i][7].ToString() + "','" +
                        _currCreatorId + "','" +
                        ds.Tables[0].Rows[i][9].ToString() + "','" +
                        ds.Tables[0].Rows[i][10].ToString() + "','" +
                        _date + "','" +
                        _isDeleted + "') END TRY " +
                        "BEGIN CATCH " +
                        "IF ERROR_NUMBER() = 2627 " +
                        "UPDATE Ticket SET " +
                        "Title ='" + ds.Tables[0].Rows[i][1].ToString() + "'," +
                        "Description ='" + ds.Tables[0].Rows[i][2].ToString() + "'," +
                        "TicketCategoryId ='" + ds.Tables[0].Rows[i][3].ToString() + "'," +
                        "TicketApplicationId ='" + ds.Tables[0].Rows[i][4].ToString() + "'," +
                        "TicketStatusId ='" + ds.Tables[0].Rows[i][5].ToString() + "'," +
                        "TicketPriorityId ='" + ds.Tables[0].Rows[i][6].ToString() + "'," +
                        "DueDate ='" + ds.Tables[0].Rows[i][7].ToString() + "', " +
                        //"CreatorId ='" + ds.Tables[0].Rows[i][8].ToString() + "', " +
                        //"SolutionId ='" + ds.Tables[0].Rows[i][9].ToString() + "', " +
                        "SolutionId ='" + _solutionId + "', " +
                        "CreatedAt ='" + ds.Tables[0].Rows[i][10].ToString() + "', " +
                        "LastUpdateTime ='" + _date + "', " +
                        "IsDeleted ='" + _isDeleted + "' " +
                        "Where Id='" + ds.Tables[0].Rows[i][0].ToString() + "' END CATCH " +
                        "SET IDENTITY_INSERT Ticket OFF ";
                    con.Open();
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            return View();
        }
        #endregion

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            ViewBag.Id = id;
            if (ticket == null)
            {
                return HttpNotFound();
            }
            var _currUserId = User.Identity.GetUserId();
            if (ticket.CreatorId != _currUserId && User.IsInRole("User"))
            {
                return HttpNotFound();
            }
            return View(ticket);
        }
        [PermissionAuthorization("user_crud")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                ticket.CreatorId = User.Identity.GetUserId();

                db.Tickets.Add(ticket);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new TicketEventArgs(ticket, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan Ticket";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = ticket.Id});
            }

            TempData["Alert"] = "Gagal menambahkan Ticket. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(ticket);
        }
        [PermissionAuthorization("user_crud")]
        public ActionResult Edit(int? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            ViewBag.Id = id;
            var _currUserId = User.Identity.GetUserId();
            if (ticket == null)
            {
                return HttpNotFound();
            }
            if (ticket.CreatorId != _currUserId && User.IsInRole("User"))
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("user_crud")]
        public ActionResult Edit(Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticket).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new TicketEventArgs(ticket, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan Ticket";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = ticket.Id});
            }
            ViewBag.Id = ticket.Id;

            TempData["Alert"] = "Gagal melakukan perubahan Ticket. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(ticket);
        }
        [PermissionAuthorization("administrator_only")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Ticket ticket = db.Tickets.Find(id);
            ViewBag.Id = id;

            if (ticket == null)
            {
                return HttpNotFound();
            }
            var _currUserId = User.Identity.GetUserId();
            if (ticket.CreatorId != _currUserId && User.IsInRole("User"))
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("administrator_only")]
        public ActionResult DeleteConfirmed(int id)
        {
            Ticket ticket = db.Tickets.Find(id);
            ViewBag.Id = id;
            db.Tickets.Remove(ticket);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketEventArgs(ticket, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
