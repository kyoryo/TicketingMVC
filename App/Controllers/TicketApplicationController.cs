﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;
using App.Infrastructure;

namespace App.Controllers
{
    [PermissionAuthorization("administrator_only")]
    public class TicketApplicationEventArgs : EventArgs
    {
        public readonly TicketApplication After;
        public readonly CrudEventEnumStatus Status;

        public TicketApplicationEventArgs(TicketApplication after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class TicketApplicationController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<TicketApplicationEventArgs> AfterCall;

        protected virtual void OnCrudOperation(TicketApplicationEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        [PermissionAuthorization("read_master_data")]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TicketApplication ticketApplication = db.TicketApplications.Find(id);
            ViewBag.Id = id;
            if (ticketApplication == null)
            {
                return HttpNotFound();
            }
            return View(ticketApplication);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TicketApplication ticketApplication)
        {
            if (ModelState.IsValid)
            {
                db.TicketApplications.Add(ticketApplication);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new TicketApplicationEventArgs(ticketApplication, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan TicketApplication";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = ticketApplication.Id});
            }

            TempData["Alert"] = "Gagal menambahkan TicketApplication. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(ticketApplication);
        }
        [PermissionAuthorization("administrator_only")]
        //[Authorize(User)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TicketApplication ticketApplication = db.TicketApplications.Find(id);
            ViewBag.Id = id;

            if (ticketApplication == null)
            {
                return HttpNotFound();
            }

            return View(ticketApplication);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("administrator_only")]
        public ActionResult Edit(TicketApplication ticketApplication)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticketApplication).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new TicketApplicationEventArgs(ticketApplication, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan TicketApplication";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = ticketApplication.Id});
            }
            ViewBag.Id = ticketApplication.Id;

            TempData["Alert"] = "Gagal melakukan perubahan TicketApplication. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(ticketApplication);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            TicketApplication ticketApplication = db.TicketApplications.Find(id);
            ViewBag.Id = id;

            if (ticketApplication == null)
            {
                return HttpNotFound();
            }
            return View(ticketApplication);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TicketApplication ticketApplication = db.TicketApplications.Find(id);
            ViewBag.Id = id;
            db.TicketApplications.Remove(ticketApplication);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketApplicationEventArgs(ticketApplication, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
