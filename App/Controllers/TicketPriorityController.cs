﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;
using App.Infrastructure;

namespace App.Controllers
{
    [PermissionAuthorization("administrator_only")]
    public class TicketPriorityEventArgs : EventArgs
    {
        public readonly TicketPriority After;
        public readonly CrudEventEnumStatus Status;

        public TicketPriorityEventArgs(TicketPriority after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class TicketPriorityController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<TicketPriorityEventArgs> AfterCall;

        protected virtual void OnCrudOperation(TicketPriorityEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }
        [PermissionAuthorization("read_master_data")]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorization("administrator_only")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TicketPriority ticketPriority = db.TicketPriorities.Find(id);
            ViewBag.Id = id;
            if (ticketPriority == null)
            {
                return HttpNotFound();
            }
            return View(ticketPriority);
        }

        [PermissionAuthorization("administrator_only")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TicketPriority ticketPriority)
        {
            if (ModelState.IsValid)
            {
                db.TicketPriorities.Add(ticketPriority);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new TicketPriorityEventArgs(ticketPriority, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan TicketPriority";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = ticketPriority.Id});
            }

            TempData["Alert"] = "Gagal menambahkan TicketPriority. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(ticketPriority);
        }

        [PermissionAuthorization("administrator_only")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TicketPriority ticketPriority = db.TicketPriorities.Find(id);
            ViewBag.Id = id;

            if (ticketPriority == null)
            {
                return HttpNotFound();
            }

            return View(ticketPriority);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("administrator_only")]
        public ActionResult Edit(TicketPriority ticketPriority)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticketPriority).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new TicketPriorityEventArgs(ticketPriority, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan TicketPriority";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = ticketPriority.Id});
            }
            ViewBag.Id = ticketPriority.Id;

            TempData["Alert"] = "Gagal melakukan perubahan TicketPriority. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(ticketPriority);
        }

        [PermissionAuthorization("administrator_only")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            TicketPriority ticketPriority = db.TicketPriorities.Find(id);
            ViewBag.Id = id;

            if (ticketPriority == null)
            {
                return HttpNotFound();
            }
            return View(ticketPriority);
        }
        [PermissionAuthorization("administrator_only")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TicketPriority ticketPriority = db.TicketPriorities.Find(id);
            ViewBag.Id = id;
            db.TicketPriorities.Remove(ticketPriority);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketPriorityEventArgs(ticketPriority, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
