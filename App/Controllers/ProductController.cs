﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Infrastructure;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class ProductEventArgs : EventArgs
    {
        public readonly Product After;
        public readonly CrudEventEnumStatus Status;

        public ProductEventArgs(Product after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    [PermissionAuthorization("read_master_data")]
    public class ProductController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<ProductEventArgs> AfterCall;

        protected virtual void OnCrudOperation(ProductEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            ViewBag.Id = id;
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        [PermissionAuthorization("crud_master_data")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("crud_master_data")]
        public ActionResult Create(Product product)
        {
            if (ModelState.IsValid)
            {
                product.Id = Guid.NewGuid();
                db.Products.Add(product);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new ProductEventArgs(product, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan Product";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = product.Id});
            }

            TempData["Alert"] = "Gagal menambahkan Product. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(product);
        }

        [PermissionAuthorization("crud_master_data")]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            ViewBag.Id = id;

            if (product == null)
            {
                return HttpNotFound();
            }

            return View(product);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("crud_master_data")]
        public ActionResult Edit(Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new ProductEventArgs(product, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan Product";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = product.Id});
            }
            ViewBag.Id = product.Id;

            TempData["Alert"] = "Gagal melakukan perubahan Product. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(product);
        }

        [PermissionAuthorization("crud_master_data")]
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Product product = db.Products.Find(id);
            ViewBag.Id = id;

            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("crud_master_data")]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Product product = db.Products.Find(id);
            ViewBag.Id = id;
            db.Products.Remove(product);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new ProductEventArgs(product, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
