﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;
using App.Infrastructure;

namespace App.Controllers
{
    [PermissionAuthorization("administrator_only")]
    public class TicketCategoryEventArgs : EventArgs
    {
        public readonly TicketCategory After;
        public readonly CrudEventEnumStatus Status;

        public TicketCategoryEventArgs(TicketCategory after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class TicketCategoryController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<TicketCategoryEventArgs> AfterCall;

        protected virtual void OnCrudOperation(TicketCategoryEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }
        [PermissionAuthorization("read_master_data")]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TicketCategory ticketCategory = db.TicketCategories.Find(id);
            ViewBag.Id = id;
            if (ticketCategory == null)
            {
                return HttpNotFound();
            }
            return View(ticketCategory);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("administrator_only")]
        public ActionResult Create(TicketCategory ticketCategory)
        {
            if (ModelState.IsValid)
            {
                db.TicketCategories.Add(ticketCategory);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new TicketCategoryEventArgs(ticketCategory, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan TicketCategory";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = ticketCategory.Id});
            }

            TempData["Alert"] = "Gagal menambahkan TicketCategory. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(ticketCategory);
        }
        [PermissionAuthorization("administrator_only")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TicketCategory ticketCategory = db.TicketCategories.Find(id);
            ViewBag.Id = id;

            if (ticketCategory == null)
            {
                return HttpNotFound();
            }

            return View(ticketCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("administrator_only")]
        public ActionResult Edit(TicketCategory ticketCategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticketCategory).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new TicketCategoryEventArgs(ticketCategory, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan TicketCategory";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = ticketCategory.Id});
            }
            ViewBag.Id = ticketCategory.Id;

            TempData["Alert"] = "Gagal melakukan perubahan TicketCategory. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(ticketCategory);
        }
        [PermissionAuthorization("administrator_only")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            TicketCategory ticketCategory = db.TicketCategories.Find(id);
            ViewBag.Id = id;

            if (ticketCategory == null)
            {
                return HttpNotFound();
            }
            return View(ticketCategory);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TicketCategory ticketCategory = db.TicketCategories.Find(id);
            ViewBag.Id = id;
            db.TicketCategories.Remove(ticketCategory);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketCategoryEventArgs(ticketCategory, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
