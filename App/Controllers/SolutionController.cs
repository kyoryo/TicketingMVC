﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;
using App.Infrastructure;
using System.Xml;
using System.Configuration;
using System.Data.SqlClient;

namespace App.Controllers
{
    public class SolutionEventArgs : EventArgs
    {
        public readonly Solution After;
        public readonly CrudEventEnumStatus Status;

        public SolutionEventArgs(Solution after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class SolutionController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<SolutionEventArgs> AfterCall;

        protected virtual void OnCrudOperation(SolutionEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        [PermissionAuthorization("support_read")]
        public ActionResult Index()
        {
            
             return View();        
        }
        
        public ActionResult IndexUser()
        {
            return View();
        }

        #region testIMPORT
        /// <summary>
        /// actionresult for import - using SQL syntax
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            DataSet ds = new DataSet();
            if (Request.Files["file"].ContentLength > 0)
            {
                string FileExtension = System.IO.Path.GetExtension(Request.Files["file"].FileName);
                if (FileExtension == ".xls" || FileExtension == ".xlsx")
                {
                    string fileLocation = Server.MapPath("~/Content/") + Request.Files["file"].FileName;
                    //if there is a same file name in file location, delete it
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files["file"].SaveAs(fileLocation);
                    
                    //init connection string
                    string excelConnectionString = string.Empty;
                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";

                    //connection string for xls format
                    if (FileExtension == ".xls")
                    {
                        excelConnectionString = "Provider= Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=yes;IMEX=2\"";
                    }
                    //connection string for xlsx format
                    if (FileExtension == ".xlsx")
                    {
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }


                    //create connection to workbook and OLEDB namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelsheets = new String[dt.Rows.Count];
                    int t = 0;

                    //excel data saved in temp files
                    foreach (DataRow row in dt.Rows)
                    {
                        excelsheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);

                    string query = string.Format("Select * from [{0}]", excelsheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    } 
                }
                if (FileExtension.ToString().ToLower().Equals(".xml")){
                    string fileLocation = Server.MapPath("~/Content/")+ Request.Files["FileUpload"].FileName;
                    if (System.IO.File.Exists(fileLocation)){
                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files["FileUpload"].SaveAs(fileLocation);
                    XmlTextReader xmlreader = new XmlTextReader(fileLocation);

                    ds.ReadXml(xmlreader);
                    xmlreader.Close();
                }
                //conection using SQL
                for (int i=0;i<ds.Tables[0].Rows.Count;i++)
                {
                    var _isDeleted = string.Empty;
                    _isDeleted = ds.Tables[0].Rows[i][7].ToString();
                    if (string.IsNullOrWhiteSpace(_isDeleted)) { _isDeleted = false.ToString(); }
                    string conn = ConfigurationManager.ConnectionStrings["ScaffoldTR"].ConnectionString;
                    SqlConnection con = new SqlConnection(conn);
                    string query = "SET IDENTITY_INSERT Solution ON " +
                        "BEGIN TRY INSERT into Solution(Id, Title, Content, CreatorId, CreatedAt, LastUpdateTime, IsPublished, IsDeleted) Values('" +
                        ds.Tables[0].Rows[i][0].ToString() + "','" +
                        ds.Tables[0].Rows[i][1].ToString() + "','" +
                        ds.Tables[0].Rows[i][2].ToString() + "','" +
                        ds.Tables[0].Rows[i][3].ToString() + "','" +
                        ds.Tables[0].Rows[i][4].ToString() + "','" +
                        ds.Tables[0].Rows[i][5].ToString() + "','" +
                        ds.Tables[0].Rows[i][6].ToString() + "','" +
                        _isDeleted + "') END TRY " +
                        "BEGIN CATCH " +
                        "IF ERROR_NUMBER() = 2627 " +
                        "UPDATE Solution SET " +
                        "Title ='" + ds.Tables[0].Rows[i][1].ToString() + "'," +
                        "Content ='" + ds.Tables[0].Rows[i][2].ToString() + "'," +
                        "CreatorId ='" + ds.Tables[0].Rows[i][3].ToString() + "'," +
                        "CreatedAt ='" + ds.Tables[0].Rows[i][4].ToString() + "'," +
                        "LastUpdateTime ='" + ds.Tables[0].Rows[i][5].ToString() + "'," +
                        "IsPublished ='" + ds.Tables[0].Rows[i][6].ToString() + "', " +
                        "IsDeleted ='" + ds.Tables[0].Rows[i][7].ToString() + "' " +
                        "Where Id='" + ds.Tables[0].Rows[i][0].ToString() + "' END CATCH " +
                        "SET IDENTITY_INSERT Solution OFF ";
                    con.Open();
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            return View();
        }
        #endregion

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Solution solution = db.Solutions.Find(id);
            ViewBag.Id = id;
            if (solution == null)
            {
                return HttpNotFound();
            }
            return View(solution);
        }

        [PermissionAuthorization("support_crud")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("support_crud")]
        public ActionResult Create(Solution solution)
        {
            if (ModelState.IsValid)
            {
                solution.CreatorId = User.Identity.GetUserId();
                db.Solutions.Add(solution);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new SolutionEventArgs(solution, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan Solution";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = solution.Id});
            }

            TempData["Alert"] = "Gagal menambahkan Solution. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(solution);
        }

        [PermissionAuthorization("support_crud")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Solution solution = db.Solutions.Find(id);
            ViewBag.Id = id;

            if (solution == null)
            {
                return HttpNotFound();
            }

            return View(solution);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("support_crud")]
        public ActionResult Edit(Solution solution)
        {
            if (ModelState.IsValid)
            {
                db.Entry(solution).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new SolutionEventArgs(solution, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan Solution";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = solution.Id});
            }
            ViewBag.Id = solution.Id;

            TempData["Alert"] = "Gagal melakukan perubahan Solution. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(solution);
        }
        [PermissionAuthorization("administrator_only")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Solution solution = db.Solutions.Find(id);
            ViewBag.Id = id;

            if (solution == null)
            {
                return HttpNotFound();
            }
            return View(solution);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Solution solution = db.Solutions.Find(id);
            ViewBag.Id = id;
            db.Solutions.Remove(solution);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new SolutionEventArgs(solution, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
