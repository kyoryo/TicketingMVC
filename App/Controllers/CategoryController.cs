﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Infrastructure;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class CategoryEventArgs : EventArgs
    {
        public readonly Category After;
        public readonly CrudEventEnumStatus Status;

        public CategoryEventArgs(Category after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    [PermissionAuthorization("read_master_data")]
    public class CategoryController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<CategoryEventArgs> AfterCall;

        protected virtual void OnCrudOperation(CategoryEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            ViewBag.Id = id;
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        [PermissionAuthorization("crud_master_data")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("crud_master_data")]
        public ActionResult Create(Category category)
        {
            if (ModelState.IsValid)
            {
                db.Categories.Add(category);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new CategoryEventArgs(category, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan Category";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = category.Id});
            }

            TempData["Alert"] = "Gagal menambahkan Category. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(category);
        }

        [PermissionAuthorization("crud_master_data")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            ViewBag.Id = id;

            if (category == null)
            {
                return HttpNotFound();
            }

            return View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("crud_master_data")]
        public ActionResult Edit(Category category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(category).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new CategoryEventArgs(category, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan Category";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = category.Id});
            }
            ViewBag.Id = category.Id;

            TempData["Alert"] = "Gagal melakukan perubahan Category. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(category);
        }

        [PermissionAuthorization("crud_master_data")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Category category = db.Categories.Find(id);
            ViewBag.Id = id;

            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("crud_master_data")]
        public ActionResult DeleteConfirmed(int id)
        {
            Category category = db.Categories.Find(id);
            ViewBag.Id = id;
            db.Categories.Remove(category);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new CategoryEventArgs(category, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
