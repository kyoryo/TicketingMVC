﻿using System;
using System.Data.Entity;
using System.Net;
using System.Web.Mvc;
using App.Infrastructure;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class AudienceEventArgs : EventArgs
    {
        public readonly Audience After;
        public readonly CrudEventEnumStatus Status;

        public AudienceEventArgs(Audience after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    [PermissionAuthorization("administrator_only")]
    public class AudienceController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<AudienceEventArgs> AfterCall;

        protected virtual void OnCrudOperation(AudienceEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Audience audience = db.Audiences.Find(id);
            ViewBag.Id = id;
            if (audience == null)
            {
                return HttpNotFound();
            }
            return View(audience);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AudienceModel audience)
        {
            if (ModelState.IsValid)
            {
                var result = AudiencesStore.AddAudience(audience.Name);

                OnCrudOperation(new AudienceEventArgs(result, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = result.ClientId });
            }

            return View(audience);
        }

        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var audience = AudiencesStore.FindAudience(id);
            ViewBag.Id = id;

            if (audience == null)
            {
                return HttpNotFound();
            }

            return View(audience);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Audience audience)
        {
            if (ModelState.IsValid)
            {
                var temp = AudiencesStore.FindAudience(audience.ClientId);
                temp.Name = audience.Name;
                db.Entry(temp).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new AudienceEventArgs(temp, CrudEventEnumStatus.Update));

                return RedirectToAction("Details", new { id = temp.ClientId });
            }
            ViewBag.Id = audience.ClientId;

            return View(audience);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
