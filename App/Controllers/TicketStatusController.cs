﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;
using App.Infrastructure;

namespace App.Controllers
{
    [PermissionAuthorization("administrator_only")]
    public class TicketStatusEventArgs : EventArgs
    {
        public readonly TicketStatus After;
        public readonly CrudEventEnumStatus Status;

        public TicketStatusEventArgs(TicketStatus after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class TicketStatusController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<TicketStatusEventArgs> AfterCall;

        protected virtual void OnCrudOperation(TicketStatusEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }
        [PermissionAuthorization("read_master_data")]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TicketStatus ticketStatus = db.TicketStatuses.Find(id);
            ViewBag.Id = id;
            if (ticketStatus == null)
            {
                return HttpNotFound();
            }
            return View(ticketStatus);
        }
        [PermissionAuthorization("administrator_only")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("administrator_only")]
        public ActionResult Create(TicketStatus ticketStatus)
        {
            if (ModelState.IsValid)
            {
                db.TicketStatuses.Add(ticketStatus);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new TicketStatusEventArgs(ticketStatus, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan TicketStatus";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = ticketStatus.Id});
            }

            TempData["Alert"] = "Gagal menambahkan TicketStatus. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(ticketStatus);
        }
        [PermissionAuthorization("administrator_only")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TicketStatus ticketStatus = db.TicketStatuses.Find(id);
            ViewBag.Id = id;

            if (ticketStatus == null)
            {
                return HttpNotFound();
            }

            return View(ticketStatus);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("administrator_only")]
        public ActionResult Edit(TicketStatus ticketStatus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticketStatus).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new TicketStatusEventArgs(ticketStatus, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan TicketStatus";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = ticketStatus.Id});
            }
            ViewBag.Id = ticketStatus.Id;

            TempData["Alert"] = "Gagal melakukan perubahan TicketStatus. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(ticketStatus);
        }
        [PermissionAuthorization("administrator_only")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            TicketStatus ticketStatus = db.TicketStatuses.Find(id);
            ViewBag.Id = id;

            if (ticketStatus == null)
            {
                return HttpNotFound();
            }
            return View(ticketStatus);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("administrator_only")]
        public ActionResult DeleteConfirmed(int id)
        {
            TicketStatus ticketStatus = db.TicketStatuses.Find(id);
            ViewBag.Id = id;
            db.TicketStatuses.Remove(ticketStatus);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketStatusEventArgs(ticketStatus, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
