﻿using System.Web;
using System.Web.Mvc;
using App.Infrastructure;
using Microsoft.AspNet.Identity.Owin;

namespace App.Controllers
{
    public class BaseUserController : Controller
    {
        private readonly ApplicationRoleManager _AppRoleManager = null;
        private readonly ApplicationUserManager _AppUserManager = null;

        protected ApplicationUserManager AppUserManager
        {
            get { return _AppUserManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        }

        protected ApplicationRoleManager AppRoleManager
        {
            get { return _AppRoleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>(); }
        }
    }
}