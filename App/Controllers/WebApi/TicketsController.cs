﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class TicketsControllerEventArgs : EventArgs
    {
        public readonly Ticket After;
        public readonly CrudEventEnumStatus Status;

        public TicketsControllerEventArgs(Ticket after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class TicketsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<TicketsControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(TicketsControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/Tickets
        public DataSourceResult GetTickets(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);
            
            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }
            var _currUserId = User.Identity.GetUserId();
            var result = db.Tickets.Include(x => x.Comments).Include(x => x.Creator).Include(x => x.Solution).Include(x => x.TicketApplication).Include(x => x.TicketCategory).Include(x => x.TicketPriority).Include(x => x.TicketStatus).AsQueryable();
            if (User.IsInRole("User")) {
                result = db.Tickets.Where(x => x.CreatorId == _currUserId).Include(x => x.Comments).Include(x => x.Creator).Include(x => x.Solution).Include(x => x.TicketApplication).Include(x => x.TicketCategory).Include(x => x.TicketPriority).Include(x => x.TicketStatus).AsQueryable();            
            }
            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<TicketViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/Tickets/5
        [ResponseType(typeof(TicketViewModel))]
        public IHttpActionResult GetTicket(int id)
        {
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Ticket, TicketViewModel>(ticket));
        }

        // PUT: api/Tickets/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTicket(int id, TicketViewModel ticket)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ticket.Id)
            {
                return BadRequest();
            }

            var tempticket = ticket.ToEntity();

            db.Entry(tempticket).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new TicketsControllerEventArgs(tempticket, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TicketExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tickets
        [ResponseType(typeof(TicketViewModel))]
        public IHttpActionResult PostTicket(TicketViewModel ticket)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempticket = ticket.ToEntity();

            ticket.CreatorId = User.Identity.GetUserId();

            db.Tickets.Add(tempticket);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketsControllerEventArgs(tempticket, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = ticket.Id }, 
                    Mapper.Map<Ticket, TicketViewModel>(tempticket));
        }

        // DELETE: api/Tickets/5
        [ResponseType(typeof(TicketViewModel))]
        public IHttpActionResult DeleteTicket(int id)
        {
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return NotFound();
            }

            db.Tickets.Remove(ticket);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketsControllerEventArgs(ticket, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<Ticket, TicketViewModel>(ticket));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TicketExists(int id)
        {
            return db.Tickets.Count(e => e.Id == id) > 0;
        }
    }
}