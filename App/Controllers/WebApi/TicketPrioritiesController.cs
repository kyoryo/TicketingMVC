﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class TicketPrioritiesControllerEventArgs : EventArgs
    {
        public readonly TicketPriority After;
        public readonly CrudEventEnumStatus Status;

        public TicketPrioritiesControllerEventArgs(TicketPriority after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class TicketPrioritiesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<TicketPrioritiesControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(TicketPrioritiesControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/TicketPriorities
        public DataSourceResult GetTicketPriorities(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.TicketPriorities.AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<TicketPriorityViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/TicketPriorities/5
        [ResponseType(typeof(TicketPriorityViewModel))]
        public IHttpActionResult GetTicketPriority(int id)
        {
            TicketPriority ticketPriority = db.TicketPriorities.Find(id);
            if (ticketPriority == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<TicketPriority, TicketPriorityViewModel>(ticketPriority));
        }

        // PUT: api/TicketPriorities/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTicketPriority(int id, TicketPriorityViewModel ticketPriority)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ticketPriority.Id)
            {
                return BadRequest();
            }

            var tempticketPriority = ticketPriority.ToEntity();

            db.Entry(tempticketPriority).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new TicketPrioritiesControllerEventArgs(tempticketPriority, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TicketPriorityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TicketPriorities
        [ResponseType(typeof(TicketPriorityViewModel))]
        public IHttpActionResult PostTicketPriority(TicketPriorityViewModel ticketPriority)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempticketPriority = ticketPriority.ToEntity();

            db.TicketPriorities.Add(tempticketPriority);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketPrioritiesControllerEventArgs(tempticketPriority, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = ticketPriority.Id }, 
                    Mapper.Map<TicketPriority, TicketPriorityViewModel>(tempticketPriority));
        }

        // DELETE: api/TicketPriorities/5
        [ResponseType(typeof(TicketPriorityViewModel))]
        public IHttpActionResult DeleteTicketPriority(int id)
        {
            TicketPriority ticketPriority = db.TicketPriorities.Find(id);
            if (ticketPriority == null)
            {
                return NotFound();
            }

            db.TicketPriorities.Remove(ticketPriority);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketPrioritiesControllerEventArgs(ticketPriority, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<TicketPriority, TicketPriorityViewModel>(ticketPriority));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TicketPriorityExists(int id)
        {
            return db.TicketPriorities.Count(e => e.Id == id) > 0;
        }
    }
}