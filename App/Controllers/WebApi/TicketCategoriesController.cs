﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class TicketCategoriesControllerEventArgs : EventArgs
    {
        public readonly TicketCategory After;
        public readonly CrudEventEnumStatus Status;

        public TicketCategoriesControllerEventArgs(TicketCategory after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class TicketCategoriesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<TicketCategoriesControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(TicketCategoriesControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/TicketCategories
        public DataSourceResult GetTicketCategories(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.TicketCategories.AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<TicketCategoryViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/TicketCategories/5
        [ResponseType(typeof(TicketCategoryViewModel))]
        public IHttpActionResult GetTicketCategory(int id)
        {
            TicketCategory ticketCategory = db.TicketCategories.Find(id);
            if (ticketCategory == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<TicketCategory, TicketCategoryViewModel>(ticketCategory));
        }

        // PUT: api/TicketCategories/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTicketCategory(int id, TicketCategoryViewModel ticketCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ticketCategory.Id)
            {
                return BadRequest();
            }

            var tempticketCategory = ticketCategory.ToEntity();

            db.Entry(tempticketCategory).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new TicketCategoriesControllerEventArgs(tempticketCategory, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TicketCategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TicketCategories
        [ResponseType(typeof(TicketCategoryViewModel))]
        public IHttpActionResult PostTicketCategory(TicketCategoryViewModel ticketCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempticketCategory = ticketCategory.ToEntity();

            db.TicketCategories.Add(tempticketCategory);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketCategoriesControllerEventArgs(tempticketCategory, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = ticketCategory.Id }, 
                    Mapper.Map<TicketCategory, TicketCategoryViewModel>(tempticketCategory));
        }

        // DELETE: api/TicketCategories/5
        [ResponseType(typeof(TicketCategoryViewModel))]
        public IHttpActionResult DeleteTicketCategory(int id)
        {
            TicketCategory ticketCategory = db.TicketCategories.Find(id);
            if (ticketCategory == null)
            {
                return NotFound();
            }

            db.TicketCategories.Remove(ticketCategory);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketCategoriesControllerEventArgs(ticketCategory, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<TicketCategory, TicketCategoryViewModel>(ticketCategory));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TicketCategoryExists(int id)
        {
            return db.TicketCategories.Count(e => e.Id == id) > 0;
        }
    }
}