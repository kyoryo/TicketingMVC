﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class TicketApplicationsControllerEventArgs : EventArgs
    {
        public readonly TicketApplication After;
        public readonly CrudEventEnumStatus Status;

        public TicketApplicationsControllerEventArgs(TicketApplication after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class TicketApplicationsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<TicketApplicationsControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(TicketApplicationsControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/TicketApplications
        public DataSourceResult GetTicketApplications(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.TicketApplications.AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<TicketApplicationViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/TicketApplications/5
        [ResponseType(typeof(TicketApplicationViewModel))]
        public IHttpActionResult GetTicketApplication(int id)
        {
            TicketApplication ticketApplication = db.TicketApplications.Find(id);
            if (ticketApplication == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<TicketApplication, TicketApplicationViewModel>(ticketApplication));
        }

        // PUT: api/TicketApplications/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTicketApplication(int id, TicketApplicationViewModel ticketApplication)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ticketApplication.Id)
            {
                return BadRequest();
            }

            var tempticketApplication = ticketApplication.ToEntity();

            db.Entry(tempticketApplication).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new TicketApplicationsControllerEventArgs(tempticketApplication, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TicketApplicationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TicketApplications
        [ResponseType(typeof(TicketApplicationViewModel))]
        public IHttpActionResult PostTicketApplication(TicketApplicationViewModel ticketApplication)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempticketApplication = ticketApplication.ToEntity();

            db.TicketApplications.Add(tempticketApplication);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketApplicationsControllerEventArgs(tempticketApplication, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = ticketApplication.Id }, 
                    Mapper.Map<TicketApplication, TicketApplicationViewModel>(tempticketApplication));
        }

        // DELETE: api/TicketApplications/5
        [ResponseType(typeof(TicketApplicationViewModel))]
        public IHttpActionResult DeleteTicketApplication(int id)
        {
            TicketApplication ticketApplication = db.TicketApplications.Find(id);
            if (ticketApplication == null)
            {
                return NotFound();
            }

            db.TicketApplications.Remove(ticketApplication);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketApplicationsControllerEventArgs(ticketApplication, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<TicketApplication, TicketApplicationViewModel>(ticketApplication));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TicketApplicationExists(int id)
        {
            return db.TicketApplications.Count(e => e.Id == id) > 0;
        }
    }
}