﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class TicketStatusControllerEventArgs : EventArgs
    {
        public readonly TicketStatus After;
        public readonly CrudEventEnumStatus Status;

        public TicketStatusControllerEventArgs(TicketStatus after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class TicketStatusController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<TicketStatusControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(TicketStatusControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/TicketStatus
        public DataSourceResult GetTicketStatus(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.TicketStatuses.AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<TicketStatusViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/TicketStatus/5
        [ResponseType(typeof(TicketStatusViewModel))]
        public IHttpActionResult GetTicketStatus(int id)
        {
            TicketStatus ticketStatus = db.TicketStatuses.Find(id);
            if (ticketStatus == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<TicketStatus, TicketStatusViewModel>(ticketStatus));
        }

        // PUT: api/TicketStatus/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTicketStatus(int id, TicketStatusViewModel ticketStatus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ticketStatus.Id)
            {
                return BadRequest();
            }

            var tempticketStatus = ticketStatus.ToEntity();

            db.Entry(tempticketStatus).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new TicketStatusControllerEventArgs(tempticketStatus, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TicketStatusExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TicketStatus
        [ResponseType(typeof(TicketStatusViewModel))]
        public IHttpActionResult PostTicketStatus(TicketStatusViewModel ticketStatus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempticketStatus = ticketStatus.ToEntity();

            db.TicketStatuses.Add(tempticketStatus);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketStatusControllerEventArgs(tempticketStatus, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = ticketStatus.Id }, 
                    Mapper.Map<TicketStatus, TicketStatusViewModel>(tempticketStatus));
        }

        // DELETE: api/TicketStatus/5
        [ResponseType(typeof(TicketStatusViewModel))]
        public IHttpActionResult DeleteTicketStatus(int id)
        {
            TicketStatus ticketStatus = db.TicketStatuses.Find(id);
            if (ticketStatus == null)
            {
                return NotFound();
            }

            db.TicketStatuses.Remove(ticketStatus);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketStatusControllerEventArgs(ticketStatus, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<TicketStatus, TicketStatusViewModel>(ticketStatus));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TicketStatusExists(int id)
        {
            return db.TicketStatuses.Count(e => e.Id == id) > 0;
        }
    }
}