﻿function showNotif(message, type, isAjax, confirm, option) {
    NotifContent = message,
    autoClose = true;
    notifContent = '<div class="alert alert-' + type + ' media fade in"><p>' + message + '</p></div>';
    method = 3000;
    position = 'top';
    container = '.topbar';
    style = 'topbar';

    if (isAjax) {
        notifContent = '<div class="alert alert-' + type + ' media fade in"><strong>Ajax request Error!<br/></strong><p>' + message + '</p></div>';
    }

    generate(position, container, notifContent, confirm, option);
}

function generate(position, container, content, confirm, option) {
    openAnimation = 'animated fadeIn';
    closeAnimation = 'animated fadeOut';

    var n = $(container).noty({
        text: content,
        dismissQueue: true,
        layout: position,
        closeWith: ['click'],
        theme: 'made',
        maxVisible: 10,
        buttons: confirm ? [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: option.okCallback
            },
            {
                addClass: 'btn btn-danger', text: 'Cancel', onClick: option.cancelCallback
            }
        ] : '',
        animation: {
            open: openAnimation,
            close: closeAnimation
        },
        timeout: method,
        callback: {
            onShow: function () {
                var sidebarWidth = $('.sidebar').width();
                var topbarHeight = $('.topbar').height();
                if (position == 'top' && style == 'topbar') {
                    $('.noty_inline_layout_container').css('top', 0);
                    if ($('body').hasClass('rtl')) {
                        $('.noty_inline_layout_container').css('right', 0);
                    }
                    else {
                        $('.noty_inline_layout_container').css('left', 0);
                    }

                }

            }
        }
    });
}

