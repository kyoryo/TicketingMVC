﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Helper
{
    public class CheckResult
    {
        public Guid Id { get; set; }
        public bool Success { get; set; }
        public int RowCount { get; set; }
        public int ErrorCount { get; set; }
        public string ErrorMessage { get; set; }
    }
}