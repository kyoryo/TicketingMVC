﻿using Domain.Models.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace App.Helper
{
    public class UploadHelper
    {
        #region Declarations
        private ITicketComment _ticketComment;
        #endregion

        #region Constructor
        public UploadHelper(ITicketComment _ticketComment)
        {
            this._ticketComment = _ticketComment;
        }
        #endregion

        #region Read Excel
        public DataSet ReadExcelFile(string filePath)
        {
            try
            {
                DataSet dsTicketComment = GetDataFromSheet(filePath);
                if (dsTicketComment.Tables[0].Rows.Count > 0)
                {
                    ValidateExcelData(dsTicketComment.Tables[0]);
                }
                return dsTicketComment;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Oh no, Error Occured in ReadExcelFile method.");
            }
        }
        #endregion

        #region CaptureExcelData
        private DataSet GetDataFromSheet(string filePath)
        {
            OleDbConnection oledbConn;
            try
            {
                string oledbConnectionString = ConfigurationManager.AppSettings["oledbConn"].ToString();
                oledbConnectionString = oledbConnectionString.Replace("path", filePath);
                oledbConn = new OleDbConnection(oledbConnectionString);
                oledbConn.Open();

                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                DataSet ds = new DataSet();
                cmd.Connection = oledbConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = cmd.CommandText = "SELECT [" +
                    Constants.Id + "],[" +
                    Constants.TicketId + "],[" +
                    Constants.CommenterId + "],[" +
                    Constants.CreatedAt + "],[" +
                    Constants.LastUpdateTime + "],[" +
                    Constants.IsDeleted + "] FROM [Sheet1$]";
                oleda.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                //
                throw new ApplicationException("Oh no, Error occured in GetDataFromSheet method");
            }
        }
        #endregion

        #region ValidateExcelData
        private void ValidateExcelData(DataTable dtUploadedExcel)
        {
            //add an extra column named Error inside the DataTable which contains the excelUploadedData
            string error = string.Empty;
            decimal number = 0;
            try
            {
                dtUploadedExcel.Columns.Add("Error");
                for (int row = 0; row < dtUploadedExcel.Rows.Count; row++)
                {
                    error = string.Empty;
                    for (int col = 0; col<dtUploadedExcel.Columns.Count -1;col++)
                    {
                        //validate mandatory fields/cols
                        if (dtUploadedExcel.Rows[row][col].ToString() == null || dtUploadedExcel.Rows[row][col].ToString() == "")
                        {
                            error += dtUploadedExcel.Columns[col].ToString() + "Should not be Null in line" + (row + 1) + ";";
                        } 

                        //adding custom validations
                        //if (dtUploadedExcel.Columns[col].ColumnName == Constants.Id)
                        //{
                        //    
                        //}

                        //adding the error details to the Error coulmn of the DataTable which can be used for displaying the error details on the Page
                        dtUploadedExcel.Rows[row]["Error"] = error;
                    }                    
                }
            }
            catch (Exception ex)
            {
                //Code to be written to log the exception details in a file;
                throw new ApplicationException("Oh no, Error occured in ValidateExcelData method");
            }

        }
        #endregion
    }
    public class Constants
    {
        #region Excel Headers
        public const string Id = "Id";
        public const string TicketId = "TicketId";
        public const string CommenterId = "CommenterId";
        public const string CreatedAt = "CreatedAt";
        public const string LastUpdateTime = "LastUpdateTime";
        public const string IsDeleted = "IsDeleted";


        #endregion
    }
}