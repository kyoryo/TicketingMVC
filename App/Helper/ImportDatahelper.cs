﻿using App.DAL;
using Domain.Models;
using LinqToExcel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace App.Helper
{
    public class ImportDatahelper
    {
        public CheckResult CheckImportData(string fileName, List<TicketComment> importTicketComment)
        {
            var result = new CheckResult();
            var targetFile = new FileInfo(fileName);
            if (!targetFile.Exists)
            {
                result.Id = Guid.NewGuid();
                result.Success = false;
                result.ErrorCount = 0;
                result.ErrorMessage = "Oh god, Imported file data doesn't exist ";
                return result;
            }
            var excelFile = new ExcelQueryFactory(fileName);

            //mapping
            excelFile.AddMapping<TicketComment>(x => x.Id, "Id");
            excelFile.AddMapping<TicketComment>(x => x.TicketId, "TicketId");
            excelFile.AddMapping<TicketComment>(x => x.Content, "Content");
            excelFile.AddMapping<TicketComment>(x => x.CommenterId, "CommenterId");
            excelFile.AddMapping<TicketComment>(x => x.CreatedAt, "CreatedAt");
            excelFile.AddMapping<TicketComment>(x => x.LastUpdateTime, "LastUpdateTime");
            excelFile.AddMapping<TicketComment>(x => x.IsDeleted, "IsDeleted");

            //sheetName
            var excelContent = excelFile.Worksheet<TicketComment>("Ticket Comment");

            int errorCount = 0;
            int rowIndex = 1;
            var importErrorMessages = new List<string>();


            foreach (var row in excelContent)
            {
                var errorMessage = new StringBuilder();
                var _ticketComment = new TicketComment();

                _ticketComment.Id = row.Id;
                _ticketComment.TicketId = row.TicketId;
                _ticketComment.Content = row.Content;
                _ticketComment.CommenterId = row.CommenterId;
                _ticketComment.CreatedAt = row.CreatedAt;
                _ticketComment.LastUpdateTime = DateTime.Now;
                _ticketComment.IsDeleted = row.IsDeleted;

                //if (string.IsNullOrWhiteSpace(row.IsDeleted))
                //{
                //    errorMessage
                //}

                //if (errorMessage.Length > 0)
                //{
                //    errorCount += 1;
                //    importErrorMessages.Add(string.Format("nomer {0} ada error: {1}{2}", rowIndex, errorMessage, "<br/>"));
                //}

                importTicketComment.Add(_ticketComment);
                rowIndex += 1;
            }

            try{
                result.Id = Guid.NewGuid();
                result.Success = errorCount.Equals(0);
                result.RowCount = importTicketComment.Count;
                result.ErrorCount = errorCount;

                string allErrorMessage = string.Empty;

                foreach (var message in importTicketComment){
                    allErrorMessage += message;
                }

                result.ErrorMessage = allErrorMessage;
                return result;
            }
            catch (Exception ex){
                throw;
            }
        }

        public void SaveImportData(IEnumerable<TicketComment> importTicketComment)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    foreach (var item in importTicketComment)
                    {
                        db.TicketComments.Add(item);
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }

}
