﻿using System;

namespace App.Models
{
    public class Logs
    {
        public int Id { get; set; }
        public int AuditLogId { get; set; }
        public string UserName { get; set; }
        public DateTime? EventDateUTC { get; set; }
        public string EventType { get; set; }
        public string TableName { get; set; }
        public string RecordId { get; set; }
        public string ColumnName { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
    }
}