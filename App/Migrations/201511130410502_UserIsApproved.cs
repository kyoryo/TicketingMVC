namespace App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserIsApproved : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IsApproved", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsApproved");
        }
    }
}
