namespace App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeventablesR : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Solution",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Content = c.String(nullable: false),
                        CreatorId = c.String(maxLength: 128),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        IsPublished = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatorId)
                .Index(t => t.CreatorId);
            
            CreateTable(
                "dbo.Ticket",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(),
                        TicketCategoryId = c.Int(),
                        TicketApplicationId = c.Int(),
                        TicketStatusId = c.Int(),
                        TicketPriorityId = c.Int(),
                        DueDate = c.DateTime(),
                        CreatorId = c.String(maxLength: 128),
                        SolutionId = c.Int(),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatorId)
                .ForeignKey("dbo.Solution", t => t.SolutionId)
                .ForeignKey("dbo.TicketApplication", t => t.TicketApplicationId)
                .ForeignKey("dbo.TicketCategory", t => t.TicketCategoryId)
                .ForeignKey("dbo.TicketPriority", t => t.TicketPriorityId)
                .ForeignKey("dbo.TicketStatus", t => t.TicketStatusId)
                .Index(t => t.TicketCategoryId)
                .Index(t => t.TicketApplicationId)
                .Index(t => t.TicketStatusId)
                .Index(t => t.TicketPriorityId)
                .Index(t => t.CreatorId)
                .Index(t => t.SolutionId);
            
            CreateTable(
                "dbo.TicketComment",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        TicketId = c.Int(nullable: false),
                        Content = c.String(nullable: false),
                        CommenterId = c.String(maxLength: 128),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CommenterId)
                .ForeignKey("dbo.Ticket", t => t.TicketId, cascadeDelete: true)
                .Index(t => t.TicketId)
                .Index(t => t.CommenterId);
            
            CreateTable(
                "dbo.TicketApplication",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        Name = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketCategory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        Name = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketPriority",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        Name = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        Name = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ticket", "TicketStatusId", "dbo.TicketStatus");
            DropForeignKey("dbo.Ticket", "TicketPriorityId", "dbo.TicketPriority");
            DropForeignKey("dbo.Ticket", "TicketCategoryId", "dbo.TicketCategory");
            DropForeignKey("dbo.Ticket", "TicketApplicationId", "dbo.TicketApplication");
            DropForeignKey("dbo.Ticket", "SolutionId", "dbo.Solution");
            DropForeignKey("dbo.Ticket", "CreatorId", "dbo.AspNetUsers");
            DropForeignKey("dbo.TicketComment", "TicketId", "dbo.Ticket");
            DropForeignKey("dbo.TicketComment", "CommenterId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Solution", "CreatorId", "dbo.AspNetUsers");
            DropIndex("dbo.TicketComment", new[] { "CommenterId" });
            DropIndex("dbo.TicketComment", new[] { "TicketId" });
            DropIndex("dbo.Ticket", new[] { "SolutionId" });
            DropIndex("dbo.Ticket", new[] { "CreatorId" });
            DropIndex("dbo.Ticket", new[] { "TicketPriorityId" });
            DropIndex("dbo.Ticket", new[] { "TicketStatusId" });
            DropIndex("dbo.Ticket", new[] { "TicketApplicationId" });
            DropIndex("dbo.Ticket", new[] { "TicketCategoryId" });
            DropIndex("dbo.Solution", new[] { "CreatorId" });
            DropTable("dbo.TicketStatus");
            DropTable("dbo.TicketPriority");
            DropTable("dbo.TicketCategory");
            DropTable("dbo.TicketApplication");
            DropTable("dbo.TicketComment");
            DropTable("dbo.Ticket");
            DropTable("dbo.Solution");
        }
    }
}
