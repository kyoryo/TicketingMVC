﻿using System.Web.Mvc;
using App.Infrastructure;

namespace App
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            
            // @todo Fix this
            //filters.Add(new HandleMenuAttribute());
        }
    }
}