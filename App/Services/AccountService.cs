﻿using System;
using System.Linq;
using App.DAL;

namespace App.Services
{
    public interface IAccountService
    {
        bool HasPermission(string userId, Guid permissionId);
        bool HasPermission(string userId, string permissionName);
        bool HasMenu(string userId, string action, string controller);
    }

    public class AccountService : IAccountService
    {
        public bool HasPermission(string userId, Guid permissionId)
        {
            using (var db = new ApplicationDbContext())
            {
                var roles = db.IdentityRoles.Where(x => x.Users.Any(u => u.UserId == userId));
                foreach (var role in roles)
                {
                    if (role.RolePermissions.Any(x => x.PermissionId == permissionId))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool HasPermission(string userId, string permissionName)
        {
            using (var db = new ApplicationDbContext())
            {
                var roles = db.IdentityRoles.Where(x => x.Users.Any(u => u.UserId == userId));
                foreach (var role in roles)
                {
                    if (role.RolePermissions.Any(x => x.Permission.Name == permissionName))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool HasMenu(string userId, string action, string controller)
        {
            using (var db = new ApplicationDbContext())
            {
                var roles = db.IdentityRoles.Where(x => x.Users.Any(u => u.UserId == userId)).AsEnumerable();
                foreach (var role in roles)
                {
                    if (role.RoleMenus.Any(x => (x.Menu.Controller == controller) || 
                        (x.Menu.SubMenu != null && x.Menu.SubMenu.Any(s => s.Controller == controller))))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}